exports.validateStrings = (exp, label) => (str) => {
  if (!str || !exp.test(str)) {
    return {
      error: true,
      message: `${label} didn\`t passed the validation.`,
    };
  }
};

exports.validateNumbers = (range, label) => (numb) => {
  if (!numb || numb < range[0] || numb > range[1]) {
    return {
      error: true,
      message: `${label} didn\`t passed the validation.`,
    };
  }
};

exports.validateFields = (req, res, validate) => {
  Object.keys(req.body).every(field => {
    if (validate[field]) {
      const errorObj = validate[field](req.body[field]);
      res.locals = {
        ...res.locals,
        ...errorObj,
      };
      return true;
    } else {
      res.locals = {
        error: true,
        message: 'Body contains odd fields',
      };
      return false;
    }
  });
};

exports.fieldsMustBePresent = (req, res, { id, ...rest }) => {
  Object.keys(rest).every(field => {
    if (req.body[field]) {
      return true;
    } else {
      res.locals = {
        error: true,
        message: `Body must contain ${field}`,
      };
      return false
    }
  })
}

exports.id = () => ({
  error: true,
  message: 'Id is not allowed to be in the body!',
});

exports.notEmpty = label => val => {
  if (!val) {
    return {
      error: true,
      message: `${label} must not be empty.`,
    };
  }
}

exports.isArray = label => arr => {
  if (!Array.isArray(arr) || !arr.length) {
    return {
      error: true,
      message: `${label} must not be an empty array.`,
    };
  }
}