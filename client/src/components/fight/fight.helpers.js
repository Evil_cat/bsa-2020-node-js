import {
  PLAYER_ONE_ATTACK,
  PLAYER_ONE_BLOCK,
  PLAYER_ONE_CRIT,
  PLAYER_TWO_ATTACK,
  PLAYER_TWO_BLOCK,
  PLAYER_TWO_CRIT,
  CRTI_COOLDOWN,
} from "./fight.constants";

const playerOneButtons = {
  attack: PLAYER_ONE_ATTACK,
  block: PLAYER_ONE_BLOCK,
  crit: PLAYER_ONE_CRIT,
};

const playerTwoButtons = {
  attack: PLAYER_TWO_ATTACK,
  block: PLAYER_TWO_BLOCK,
  crit: PLAYER_TWO_CRIT,
};

const fightStatus = {
  // object contains all pressed buttons
  pressedKeys: {},
};

// checks last crit time
const isCritOnCooldown = (lastCrit) =>
  lastCrit && Date.now() - lastCrit < CRTI_COOLDOWN;

// returns winner if someones health below or equal zero, in all other cases returns 0
const getWinner = (firstFighter, secondFighter) => {
  if (firstFighter.currentHealth <= 0) {
    return secondFighter;
  } else if (secondFighter.currentHealth <= 0) {
    return firstFighter;
  } else {
    return null;
  }
};

/**
 * Updates defender`s health if he is under attack
 * @param {Object} attacker
 * @param {Object} defender
 * @param {Object} attackerButtons
 * @param {Object} defenderButtons
 * @param {string} healthBarId
 */
const updateFighter = ({
  attacker,
  defender,
  attackerButtons,
  defenderButtons,
}, appendLogs) => {
  let dmg, type;
  if (
    !isCritOnCooldown(attacker.lastCrit) &&
    attackerButtons.crit.every((val) => fightStatus.pressedKeys[val])
  ) {

    dmg = getCritPower(attacker);
    type = 'CRIT';

    defender.currentHealth -= dmg;
    attacker.lastCrit = Date.now();

  } else if (fightStatus.pressedKeys[attackerButtons.attack]) {
    const defencePower = getBlockPower(defender);
    const hitPower = getHitPower(attacker);

    if (fightStatus.pressedKeys[defenderButtons.block]) {
      dmg = getDamage(hitPower, defencePower);
      defender.currentHealth -= dmg;
      type = 'BLOCKED';
    } else {
      type = 'HIT';
      defender.currentHealth -= hitPower;
    }
  }

  if (type) {
    appendLogs({
      type,
      attacker: attacker.name,
      defender: defender.name,
      damage: dmg,
      defenderHealth: defender.currentHealth,
    })
  }
};

const updateFight = (firstFighter, secondFighter, appendLogs) => {
  // first fighter attack, unless he is in a block stand
  !fightStatus.pressedKeys[PLAYER_ONE_BLOCK] &&
    updateFighter({
      attacker: firstFighter,
      defender: secondFighter,
      attackerButtons: playerOneButtons,
      defenderButtons: playerTwoButtons,
      healthBarId: "right-fighter-indicator",
    }, appendLogs);

  // second fighter attack, unless he is in a block stand
  !fightStatus.pressedKeys[PLAYER_TWO_BLOCK] &&
    updateFighter({
      attacker: secondFighter,
      defender: firstFighter,
      attackerButtons: playerTwoButtons,
      defenderButtons: playerOneButtons,
      healthBarId: "left-fighter-indicator",
    }, appendLogs);
};

const onKeyUp = ({ code }) => {
  fightStatus.pressedKeys[code] = false;
};

export function fight(firstFighter, secondFighter, appendLogs) {
  firstFighter.currentHealth = firstFighter.health;
  firstFighter.lastCrit = null;
  secondFighter.currentHealth = secondFighter.health;
  secondFighter.lastCrit = null;

  return new Promise((resolve) => {
    window.addEventListener("keydown", function onKeyDown({ code }) {
      fightStatus.pressedKeys[code] = true;
      updateFight(firstFighter, secondFighter, appendLogs);
      const winner = getWinner(firstFighter, secondFighter);

      if (winner) {
        window.removeEventListener("keydown", onKeyDown);
        window.removeEventListener("keyup", onKeyUp);
        resolve(winner);
      }
    });
    window.addEventListener("keyup", onKeyUp);
  });
}

/**
 * Returns blocked damage
 * @param {number} attack
 * @param {number} defence
 */
export function getDamage(attack, defence) {
  const damage = attack - defence;

  if (damage < 0) {
    return 0;
  }
  return damage;
}

/**
 * Get fighter`s crit damage
 * @param {Object} fighter
 */
export function getCritPower({ power }) {
  return 2 * power;
}

/**
 * Get fighter`s hit damage
 * @param {Object} fighter
 */
export function getHitPower({ power }) {
  return power * (1 + Math.random());
}

/**
 * Get fighter`s block
 * @param {Object} fighter
 */
export function getBlockPower({ defense }) {
  return defense * (1 + Math.random());
}
