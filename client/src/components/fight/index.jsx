import React from 'react';

import { getFighters, createFighter } from '../../services/domainRequest/fightersRequest';
import { getFights, createFight, putFight } from '../../services/domainRequest/fightRequest';

import NewFighter from '../newFighter';
import FightHistory from '../fightHistory';
import Fighter from '../fighter';
import { Button } from '@material-ui/core';

import { fight as fightFunction } from './fight.helpers';

import './fight.css'

class Fight extends React.Component {
    state = {
        fighters: [],
        fighter1: null,
        fighter2: null,
        fights: [],
        currFight: null,
        currLogs: [],
        isFight: false,
    };

    async componentDidMount() {
        const fighters = await getFighters();
        if(fighters && !fighters.error) {
            this.setState({ fighters });
        }
        const fights = await getFights();
        if(fights && !fights.error) {
            this.setState({ fights });
        }
    }

    appendLogs = (data) => {
        console.log(data);
        this.setState({ currLogs: [...this.state.currLogs, data] });
    }

    onFightStart = async () => {
        const { fighter1, fighter2, isFight } = this.state;
        if (fighter1 && fighter2 && !isFight) {
            const currFight = await createFight({ fighter1, fighter2 });
            this.setState({ currFight, isFight: true });

            const winner = await fightFunction(fighter1, fighter2, this.appendLogs);
            alert(winner.name);
            const updatedCurrFight = { log: this.state.currLogs, winner: winner.id };

            const fight = await putFight(updatedCurrFight, currFight.id);
            this.setState({ fights: [...this.state.fights, fight], currFight: null, currLogs: [], isFight: false });
        }
    }

    onCreate = (fighter) => {
        this.setState({ fighters: [...this.state.fighters, fighter] });
    }

    onFighter1Select = (fighter1) => {
        this.setState({ fighter1 });
    }

    onFighter2Select = (fighter2) => {
        this.setState({ fighter2 });
    }

    getFighter1List = () => {
        const { fighter2, fighters } = this.state;
        if(!fighter2) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter2.id);
    }

    getFighter2List = () => {
        const { fighter1, fighters } = this.state;
        if(!fighter1) {
            return fighters;
        }

        return fighters.filter(it => it.id !== fighter1.id);
    }

    render() {
        const  { fighter1, fighter2, isFight, fights, currFight, currLogs } = this.state;
        const fightsProp = currFight ? [...fights, { ...currFight, log: currLogs }] : fights;
        return (
            <div id="wrapper">
                <NewFighter onCreated={this.onCreate} />
                <div class="figh-wrapper">
                    <Fighter selectedFighter={fighter1} onFighterSelect={this.onFighter1Select} fightersList={this.getFighter1List() || []} />
                    <div className="btn-wrapper">
                        <Button onClick={this.onFightStart} variant="contained" color="primary" disabled={!fighter1 || !fighter2 || isFight} >Start Fight</Button>
                    </div>
                    <Fighter selectedFighter={fighter2} onFighterSelect={this.onFighter2Select} fightersList={this.getFighter2List() || []} />
                </div>
                <div class="figh-wrapper">
                    <FightHistory fightsProp={fightsProp}/>
                </div>
            </div>
        );
    }
}

export default Fight;