export const PLAYER_ONE_ATTACK = 'KeyA';
export const PLAYER_ONE_BLOCK = 'KeyD';
export const PLAYER_ONE_CRIT = ['KeyQ', 'KeyW', 'KeyE'];

export const PLAYER_TWO_ATTACK= 'KeyJ';
export const PLAYER_TWO_BLOCK = 'KeyL';
export const PLAYER_TWO_CRIT = ['KeyU', 'KeyI', 'KeyO'];

export const CRTI_COOLDOWN = 10000;