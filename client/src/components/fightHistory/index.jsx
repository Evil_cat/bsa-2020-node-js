import React, { useState, useEffect } from 'react';
import { List, ListItem, makeStyles, ListItemText } from '@material-ui/core';

import Logs from '../logs';

const useStyles = makeStyles((theme) => ({
  historyRoot: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
  },
  historyList: {
    minWidth: "200px",
    flexDirection: "column",
    textAlign: "center"
  },
  fightInfo: {
    display: "flex",
    flexDirection: "column",
    padding: "5px",
  }
}));

export default ({ fightsProp }) => {
  const classes = useStyles();
  const [fights, setFights] = useState([]);
  const [selectedIndex, setSelectedIndex] = React.useState(0);

  useEffect(() => {
    setFights(fightsProp);
  }, [fightsProp]);

  const handleClick = (index) => (event) => {
    setSelectedIndex(index);
  };

  const renderListContent = () =>
    fights.map((fight, i) => {
      const { fighter1, fighter2 } = fight;
      return (
        <ListItem
          id={fight.id}
          button
          selected={i === selectedIndex}
          onClick={handleClick(i)}
        >
          <ListItemText primary={fighter1.name + " vs " + fighter2.name} />
        </ListItem>
      );
    });

  const renderFightInfo = () => {
    const { id, fighter1, fighter2, winner, log } = fights[selectedIndex];

    return <div className={classes.fightInfo}>
      <span>id: {id}</span>
      <span>Fighter 1: {fighter1.name}</span>
      <span>Fighter 2: {fighter2.name}</span>
      {winner && (<span>Winner id: {winner}</span>)}
      {winner && (<span>Winner name: {fighter1.id === winner ? fighter1.name : fighter2.name}</span>)}
      {log && (<Logs logsProp={log} />)}
    </div>
  }

  return (
    <div className={classes.historyRoot}>
      <div className={classes.historyList}>
        <span>Fight history</span>
        <List>{renderListContent()}</List>
      </div>
      {fights && fights.length && renderFightInfo()}
    </div>
  );
};
