import { TextField } from "material-ui"
import { createFighter } from "../../services/domainRequest/fightersRequest";
import React, { useState } from "react";
import { Button } from "@material-ui/core";
import './newFighter.css';

export default function NewFighter({ onCreated }) {
    const [name, setName] = useState();
    const [power, setPower] = useState();
    const [defense, setDefense] = useState();
    const [health, setHealth] = useState();

    const onNameChange = (event) => {
        setName(event.target.value);
    }

    const onStateChange = (setState) => (event) => {
        const value = event.target.value || event.target.value === 0 ? Number(event.target.value) : null;
        setState(value);
    }

    const onSubmit = async () => {
        const data = await createFighter({ name, power, defense, health });
        if(data && !data.error) {
            onCreated(data);
        }
    }

    return (
        <div id="new-fighter">
            <div>New Fighter</div>
            <TextField onChange={onNameChange} id="standard-basic" label="Standard" placeholder="Name"/>
            <TextField onChange={onStateChange(setPower)} id="standard-basic" label="Standard" placeholder="Power" type="number" />
            <TextField onChange={onStateChange(setDefense)} id="standard-basic" label="Standard" placeholder="Defense" type="number" />
            <TextField onChange={onStateChange(setHealth)} id="standard-basic" label="Standard" placeholder="Health" type="number" />
            <Button onClick={onSubmit} variant="contained" color="primary">Create</Button>
        </div>
    );
};