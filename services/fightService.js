const { FightRepository } = require('../repositories/fightRepository');
const fighterService = require('./fighterService');

class FightsService {

  updateFight(id, data) {
    const fight = FightRepository.getOne({ id });
    fighterService.search({ id: data.winner });
    if (!fight) {
      throw new Error('Can`t update fight. Fight doesn`t exist!');
    }
    return FightRepository.update(id, data);
  }

  createFight(data) {
    const { fighter1, fighter2 } = data;
    
    fighterService.search({ id: fighter1.id });
    fighterService.search({ id: fighter2.id });

    return FightRepository.create(data);
  }

  all() {
      const fight = FightRepository.getAll();
      if (!fight || !fight.length) {
          return [];
      }
      return fight;
  }    

  search(search) {
      const item = FightRepository.getOne(search);
      if(!item) {
        throw new Error('Can`t get fight. Fight doesn`t exist!');
      }
      return item;
  }
}

module.exports = new FightsService();