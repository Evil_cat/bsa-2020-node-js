const { UserRepository } = require('../repositories/userRepository');

class UserService {

    deleteUser(id) {
        const user = UserRepository.getOne({ id });
        if (!user) {
            throw new Error('Can`t delete user. User doesn`t exist!');
        }
        UserRepository.delete(id);
        return user;
    }

    updateUser(id, data) {
        const user = UserRepository.getOne({ id });
        if (!user) {
            throw new Error('Can`t update user. User doesn`t exist!');
        }
        const result = UserRepository.update(id, data);
        return result;
    }

    createUser(data) {
        const { phoneNumber, email } = data;

        const userPhone = UserRepository.getOne({ phoneNumber });
        const userEmail = UserRepository.getOne({ email });

        if (userPhone || userEmail) {
            throw new Error('Can`t create user. User with similar phone number or email exists in the database');
        }

        return UserRepository.create(data);
    }

    all() {
        const users = UserRepository.getAll();
        if (!users || !users.length) {
            return [];
        }
        return users;
    }    

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            throw new Error('Can`t get user. User doesn`t exist!');
        }
        return item;
    }
}

module.exports = new UserService();