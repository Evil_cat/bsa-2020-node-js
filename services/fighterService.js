const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    deleteFighter(id) {
        const fighter = FighterRepository.getOne({ id });
        if (!fighter) {
            throw new Error('Can`t delete fighter. Fighter doesn`t exist!');
        }
        FighterRepository.delete(id);
        return fighter;
    }

    updateFighter(id, data) {
        const fighter = FighterRepository.getOne({ id });
        if (!fighter) {
            throw new Error('Can`t update fighter. Fighter doesn`t exist!');
        }
        return FighterRepository.update(id, data);
    }

    createFighter(data) {
        const { name } = data;
        const fighter = FighterRepository.getOne({ name });
        if (fighter) {
            throw new Error('Can`t create new fighter. Fighter with similar name exists in database.');
        }
        return FighterRepository.create(data);
    }

    all() {
        const fighters = FighterRepository.getAll();
        if (!fighters || !fighters.length) {
            return [];
        }
        return fighters;
    }    

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            throw new Error('Can`t get fighter. Fighter doesn`t exist!');
        }
        return item;
    }
}

module.exports = new FighterService();