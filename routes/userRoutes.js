const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

const getUser = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const { id } = req.params;
      if (id) {
        res.locals.result = UserService.search({ id });
      } else {
        res.locals.result = UserService.all();
      }
    }
  } catch (e) {
    res.locals = {
      ...res.locals,
      message: e.message,
      error: true,
      status: 404,
    }
  } finally {
    next();
  }
}

const deleteUser = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const { id } = req.params;
      res.locals.result = UserService.deleteUser(id);
    }
  } catch (e) {
    res.locals = {
      ...res.locals,
      message: e.message,
      error: true,
      status: 404,
    }
  } finally {
    next();
  }
}

const createUser = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const { body } = req;
      res.locals.result = UserService.createUser(body);
    }
  } catch (e) {
    res.locals = {
      ...res.locals,
      message: e.message,
      error: true,
    }
  } finally {
    next();
  }
}

const updateUser = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const { body, params: { id } } = req;
      res.locals.result = UserService.updateUser(id, body);
    }
  } catch (e) {
    res.locals = {
      ...res.locals,
      message: e.message,
      error: true,
      status: 404,
    }
  } finally {
    next();
  }
}

router.get(['/', '/:id'], getUser, responseMiddleware);
router.post('/', createUserValid, createUser, responseMiddleware);
router.put('/:id', updateUserValid, updateUser, responseMiddleware);
router.delete('/:id', deleteUser, responseMiddleware);

router.all('*', (req, res, next) => {
  res.locals.wrongRoute = true;
  next();
}, responseMiddleware);

module.exports = router;