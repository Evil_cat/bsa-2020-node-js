const { Router } = require('express');
const FightService = require('../services/fightService');
const { createFightValid, updateFightValid } = require('../middlewares/fight.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

const getFight = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const { id } = req.params;
      if (id) {
        res.locals.result = FightService.search({ id });
      } else {
        res.locals.result = FightService.all();
      }
    }
  } catch (e) {
    res.locals = {
      ...res.locals,
      status: 404,
      message: e.message,
      error: true,
    }
  } finally {
    next();
  }
}

const createFight = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const { body } = req;
      res.locals.result = FightService.createFight(body);
    }
  } catch (e) {
    res.locals = {
      ...res.locals,
      message: e.message,
      error: true,
    }
  } finally {
    next();
  }
}

const updateFight = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const { body, params: { id } } = req;
      res.locals.result = FightService.updateFight(id, body);
    }
  } catch (e) {
    res.locals = {
      ...res.locals,
      message: e.message,
      error: true,
      status: 404,
    }
  } finally {
    next();
  }
}


router.get(['/', '/:id'], getFight, responseMiddleware);
router.post('/', createFightValid, createFight, responseMiddleware);
router.put('/:id', updateFightValid, updateFight, responseMiddleware);

router.all('*', (req, res, next) => {
  res.locals.wrongRoute = true;
  next();
}, responseMiddleware);

module.exports = router;