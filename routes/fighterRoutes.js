const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

const getFighter = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const { id } = req.params;
      if (id) {
        res.locals.result = FighterService.search({ id });
      } else {
        res.locals.result = FighterService.all();
      }
    }
  } catch (e) {
    res.locals = {
      ...res.locals,
      status: 404,
      message: e.message,
      error: true,
    }
  } finally {
    next();
  }
}

const deleteFighter = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const { id } = req.params;
      res.locals.result = FighterService.deleteFighter(id);
    }
  } catch (e) {
    res.locals = {
      ...res.locals,
      status: 404,
      message: e.message,
      error: true,
    }
  } finally {
    next();
  }
}

const createFighter = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const { body } = req;
      res.locals.result = FighterService.createFighter(body);
    }
  } catch (e) {
    res.locals = {
      ...res.locals,
      message: e.message,
      error: true,
    }
  } finally {
    next();
  }
}

const updateFighter = (req, res, next) => {
  const { error } = res.locals;
  try {
    if (!error) {
      const { body, params: { id } } = req;
      res.locals.result = FighterService.updateFighter(id, body);
    }
  } catch (e) {
    res.locals = {
      ...res.locals,
      message: e.message,
      error: true,
      status: 404,
    }
  } finally {
    next();
  }
}


router.get(['/', '/:id'], getFighter, responseMiddleware);
router.post('/', createFighterValid, createFighter, responseMiddleware);
router.put('/:id', updateFighterValid, updateFighter, responseMiddleware);
router.delete('/:id', deleteFighter, responseMiddleware);

router.all('*', (req, res, next) => {
  res.locals.wrongRoute = true;
  next();
}, responseMiddleware);


module.exports = router;