

const responseMiddleware = (req, res, next) => {
  const { wrongRoute, error, message, result, status } = res.locals;

  if (wrongRoute) {
    res.status(404).json({
      error: true,
      message: `Path ${req.path} doesn\`t exist.`,
    });
  } else if (error) {
    res.status(status || 400).json({
      error,
      message,
    });
  } else {
    res.status(200).json(result);
  }
};

exports.responseMiddleware = responseMiddleware;
