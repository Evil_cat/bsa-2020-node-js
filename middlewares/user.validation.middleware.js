const { user } = require('../models/user');
const { id, validateStrings, validateFields, fieldsMustBePresent } = require('../helpers/validate');

const createUserValidationFunctions = {
  id,
  firstName: validateStrings(user.firstName, 'First name'),
  lastName: validateStrings(user.lastName, 'Last name'),
  email: validateStrings(user.email, 'Email'),
  phoneNumber: validateStrings(user.phoneNumber, 'Phone number'),
  password: validateStrings(user.password, 'Password'),
}

const updateUserValidationFunctions = {
  ...createUserValidationFunctions,
}

exports.createUserValid = (req, res, next) => {
  fieldsMustBePresent(req, res, createUserValidationFunctions);
  if (res.locals.error)
    validateFields(req, res, createUserValidationFunctions);
  next();
};

exports.updateUserValid = (req, res, next) => {
  validateFields(req, res, updateUserValidationFunctions);
  next();
};

