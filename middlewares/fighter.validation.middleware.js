const { fighter } = require('../models/fighter');
const { id, validateStrings, validateNumbers, validateFields, fieldsMustBePresent } = require('../helpers/validate');

const createFighterValidationFunctions = {
  id,
  name: validateStrings(fighter.name, 'Name'),
  health: validateNumbers(fighter.health, 'Health'),
  power: validateNumbers(fighter.power, 'Power'),
  defense: validateNumbers(fighter.defense, 'Defense'),
}

const updateFighterValidationFunctions = {
  ...createFighterValidationFunctions,
}

exports.createFighterValid = (req, res, next) => {
  fieldsMustBePresent(req, res, createFighterValidationFunctions);
  if (!res.locals.error)
    validateFields(req, res, createFighterValidationFunctions);
  next();
}

exports.updateFighterValid = (req, res, next) => {
  validateFields(req, res, updateFighterValidationFunctions);
  next();
}
