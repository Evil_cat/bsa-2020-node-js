const { id, isArray, notEmpty, validateFields, fieldsMustBePresent } = require('../helpers/validate');

const createFightValidationFunctions = {
  id,
  fighter1: notEmpty('Fighter 1'),
  fighter2: notEmpty('Fighter 2'),
}

const updateFightValidationFunctions = {
  log: isArray('Log'),
  winner: notEmpty('Winner'),
}

exports.createFightValid = (req, res, next) => {
  fieldsMustBePresent(req, res, createFightValidationFunctions);
  if (!res.locals.error)
    validateFields(req, res, createFightValidationFunctions);
  next();
}

exports.updateFightValid = (req, res, next) => {
  validateFields(req, res, updateFightValidationFunctions);
  next();
}
