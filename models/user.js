exports.user = {
    id: '',
    firstName: /^.{2,}$/,
    lastName: /^.{2,}$/,
    email: /^[a-zA-Z0-9_.-]+@gmail\.com$/,
    phoneNumber: /^\+380[0-9]{9}$/,
    password: /^.{3,}$/ 
}
