exports.fighter = {
    id: '',
    name: /^\w{2,}$/,
    health: [10, 100],
    power: [1, 10],
    defense: [1, 10],
}